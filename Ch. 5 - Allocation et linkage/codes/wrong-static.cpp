#include <iostream>

using namespace std;

class A
{
    static int count = 1;
    int _id; //ask why _
    
    public:
        A() : _id(count)
        {
            count++;
        }
    
        int id() const
        {
            return _id;   
        }
};

int main()
{
    A a1; A a2; A a3;
    cout << a1.id() << " " << a2.id() << " " << a3.id() << endl;
}
