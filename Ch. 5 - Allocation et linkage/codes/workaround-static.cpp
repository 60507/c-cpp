#include <iostream>

using namespace std;

class A
{
    int _id;
    
    static int return_and_increment_counter() //static member function
    {
        static int counter = 0;
        counter++;
        return counter;
    }
    
    public:
        A() : _id(return_and_increment_counter())
        {
        }
    
        int id() const
        {
            return _id;   
        }            
};

int main()
{
    A a1; A a2; A a3;
    cout << a1.id() << " " << a2.id() << " " << a3.id() << endl;  
}
