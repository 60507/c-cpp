#include <iostream>

#include "static-attribute.h"

using namespace std;

int main()
{
    A a1; A a2; A a3;
    cout << a1.id() << " " << a2.id() << " " << a3.id() << endl;
}
