#include <iostream>
#include <optional>
#include <iomanip>
#include <functional>

using namespace std;

class NanInt
{
    optional<int> i;
    
    public:
        NanInt() : i(nullopt) 
        {}
    
        NanInt(int i) : i(i) {}
    
        int& value() 
        { 
            return i.value(); 
        }
    
        const int& value() const 
        { 
            return i.value(); 
        }
    
        bool is_NaN() const
        {
            return ! i.has_value();
        }
};

bool operator==(const NanInt& i1, const NanInt& i2)
{
    if(! i1.is_NaN() && ! i2.is_NaN())
        return i1.value() == i2.value();
    return false;
}

bool operator!=(const NanInt& i1, const NanInt& i2)
{
    return !(i1 == i2);   
}

bool operator<(const NanInt& i1, const NanInt& i2)
{
    if(! i1.is_NaN() && ! i2.is_NaN())
        return i1.value() < i2.value();
    return false;
}

bool operator<=(const NanInt& i1, const NanInt& i2)
{
    return i1 == i2 || i1 < i2; //potentially two comparizons with i1
}

bool operator>(const NanInt& i1, const NanInt& i2)
{
    //return !(i1 <= i2); //wrong !
    return i2 < i1;
}

bool operator>=(const NanInt& i1, const NanInt& i2)
{
    //return !(i1 < i2); //wrong !
    return i2 <= i1;
}

template<class Function>
void test_prints(Function&& f, const string& op)
{
    NanInt nan;
    NanInt i1 = 1;
    NanInt i2 = 2;
    
    cout << "Nan " << op << " Nan ?    " << f(nan, nan) << endl;
    cout << "Nan " << op << " i1  ?    " << f(nan, i1) << endl;
    cout << "i1  " << op << " Nan ?    " << f(i1, nan) << endl;
    cout << "Nan " << op << " 1   ?    " << f(nan, 1) << endl;
    cout << "1   " << op << " Nan ?    " << f(1, nan) << endl;
    cout << "i1  " << op << " i1  ?    " << f(i1, i1) << endl;
    cout << "i1  " << op << " 1   ?    " << f(i1, 1) << endl;
    cout << "1   " << op << " i1  ?    " << f(1, i1) << endl;
    cout << "i1  " << op << " i2  ?    " << f(i1, i2) << endl << endl;
}

int main()
{
    NanInt nan;
    NanInt i1 = 1;
    NanInt i2 = 2;
    cout << boolalpha;
    
    /*
    cout << (nan == nan) << endl;
    cout << (nan == i1) << endl;
    cout << (i1 == nan) << endl;
    cout << (nan == 1) << endl;
    cout << (1 == nan) << endl;
    cout << (i1 == i1) << endl;
    cout << (i1 == 1) << endl;
    cout << (1 == i1) << endl;
    cout << (i1 == i2) << endl << endl;
    */
    
    test_prints((bool (*)(const NanInt&, const NanInt&))operator==, "==");
    test_prints((bool (*)(const NanInt&, const NanInt&))operator!=, "!=");
    test_prints((bool (*)(const NanInt&, const NanInt&))operator<, "< ");
    test_prints((bool (*)(const NanInt&, const NanInt&))operator<=, "<=");
    test_prints((bool (*)(const NanInt&, const NanInt&))operator>, "> ");
    test_prints((bool (*)(const NanInt&, const NanInt&))operator>=, ">=");
}
