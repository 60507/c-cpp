#include <iostream>
#include <string>

using namespace std;

class vector2d;

vector2d operator+(const vector2d& v1, const vector2d& v2);

class vector2d
{
    double _x, _y;
    
    public:
        vector2d(double x = 0, double y = 0) : _x(x), _y(y) 
        {}
    
        double x() const { return _x; }
        double y() const { return _y; }                        
    
        vector2d& operator +=(const vector2d& v)
        {
            vector2d res = (*this) + v;
            _x = res._x;
            _y = res._y;
            
            return *this;
        }
    
        string to_string() const
        {
            string s;
            s += "(";
            s += std::to_string(_x); //ask why std:: is important
            s += " , ";
            s += std::to_string(_y);
            s += ")";
            return s;
        }            
};

vector2d operator+(const vector2d& v1, const vector2d& v2)
{
    return vector2d(v1.x() + v2.x(), v1.y() + v2.y());
}

ostream& operator <<(ostream& out, const vector2d& v) // << from to_string
{
    return out << v.to_string();
}

int main()
{
    vector2d v1(1,2); vector2d v2(3,4);
    cout << v1 << " " << v2 << endl;
    
    cout << (v1 + v2) << endl;
    v1 += v2;
    cout << v1 << endl;
    
    vector2d v3 = v1 + v2;
    cout << v3 << endl;
}
