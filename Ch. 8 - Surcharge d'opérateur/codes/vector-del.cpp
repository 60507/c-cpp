#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class vector;
ostream& operator<<(ostream& out, const vector& v);

class vector
{
    int n;
    double * tab;
    
    public:
        vector(int nbr) : n(nbr), tab(new double[n])
        {}
        
        vector(const vector & v) = delete;
        
        ~vector()
        {
            cout << "Deleting " << *this << endl;
            delete[] tab;
        }    

        vector & operator = (const vector & v) = delete;

        double & operator [] (int i)
        {
            return tab[i];
        }

        int size() const
        {
            return n;   
        }
    
        const double* c_array() const
        {
            return tab;   
        }
};

ostream& operator<<(ostream& out, const vector& v)
{
    out << "vector " << &v << " - size = " << v.size() << " " << ", array-adress : " << v.c_array() << " -> ";
    const double* tab = v.c_array();
    for(int i = 0; i < v.size(); i++)
        out << tab[i] << " ";
    return out;
}

void f(vector& v) //doesn't work without &
{
    cout << "Entering f" << endl;
    cout << v << endl;
    cout << "Exiting f" << endl;
}

int main()
{
    vector a (5);
    for(int i = 0; i < 5; i++)
        a[i] = i;

    f(a);
    cout << a << endl;

    vector b (4);
    for(int i = 0; i < 4; i++)
        b[i] = i + 1;

    cout << a << endl;
    cout << b << endl;

    //b = a;
    //cout << b << endl;
}
