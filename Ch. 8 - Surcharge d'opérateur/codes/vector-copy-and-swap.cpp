#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class vector;
void swap(vector& v1, vector& v2);
ostream& operator<<(ostream& out, const vector& v);

class vector
{
    int n;
    double * tab;
    
    public:
        vector(int nbr) : n(nbr), tab(new double[n])
        {}
        
        vector(const vector & v) : n(v.n), tab(new double[v.n])
        {            
            copy(v.tab, v.tab + v.n, tab);
        }
        
        ~vector()
        {
            cout << "Deleting " << *this << endl;
            if(tab != nullptr)
            {
                delete[] tab;
                tab = nullptr;
            }
        }    

        vector & operator = (vector v)
        {
            ::swap(*this, v);
            return *this;
        }

        double & operator [] (int i)
        {
            return tab[i];
        }

        int size() const
        {
            return n;   
        }
    
        const double* c_array() const
        {
            return tab;   
        }
    
        void swap(vector& v)
        {
            std::swap(n, v.n);
            std::swap(tab, v.tab);
        }
};

void swap(vector& v1, vector& v2)
{
    v1.swap(v2);
}

ostream& operator<<(ostream& out, const vector& v)
{
    out << "vector " << &v << " - size = " << v.size() << " " << ", array-adress : " << v.c_array() << " -> ";
    const double* tab = v.c_array();
    for(int i = 0; i < v.size(); i++)
        out << tab[i] << " ";
    return out;
}

void f(vector v)
{
    cout << "Entering f" << endl;
    cout << v << endl;
    cout << "Exiting f" << endl;
}

int main()
{
    vector a (5);
    for(int i = 0; i < 5; i++)
        a[i] = i;

    f(a);
    cout << a << endl;

    vector b (4);
    for(int i = 0; i < 4; i++)
        b[i] = i + 1;

    cout << a << endl;
    cout << b << endl;

    b = a;
    cout << b << endl;
}
