#include <iostream>

using namespace std;

class Integer
{
    int i;
    public:
        Integer(int i = 0) : i(i) {}
        
        int& value()
        {
            return i;   
        }
    
        const int& value() const
        {
            return i;   
        }

        Integer& operator ++() //prefix
        { 
            cout << "prefix" << endl; 
            i++; 
            return *this; 
        } 

        Integer operator ++(int) //suffix
        { 
            cout << "suffix" << endl; 
            Integer r = *this; 
            operator++(); 
            return r;
        }
};

ostream& operator << (ostream& out, const Integer& i)
{
    out << i.value();
}

int main()
{
    Integer i(2);
    Integer j = i;

    cout << i++ << endl;
    cout << ++j << endl;  
    
    j.value() = 3;
}
