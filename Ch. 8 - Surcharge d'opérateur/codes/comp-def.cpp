#include <iostream>
#include <compare>
#include <vector>
#include <iomanip>

using namespace std;

struct Point
{
    int x, y;
    
    bool operator==(const Point& p) const = default;
    auto operator<=>(const Point& p) const = default;
};

ostream& operator<<(ostream& out, const Point& p)
{
    return (out << "( " << p.x << " , " << p.y << " )" );
}

int main()
{
    vector<Point> v = {{1,2}, {1, 3}, {2, 1}, {3, 1}, {1, 2}};
    cout << boolalpha;
    
    for(int i = 0; i < v.size(); i++)
        for(int j = 0; j < v.size(); j++)
        {
            cout << v[i] << " == " << v[j] << " ?   " << (v[i] == v[j]) << endl;
            cout << v[i] << " <  " << v[j] << " ?   " << (v[i] < v[j]) << endl;
        }    
}
