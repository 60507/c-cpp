#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

class fraction
{
    unsigned num, denom;
    bool positive;

    public:
        fraction(int num, int denom);
        fraction(unsigned num, unsigned denom, bool positive = true);                
        
        fraction operator*(const fraction& f1) const; 
    
        fraction operator*(const int& i) const;
    
        unsigned numerator() const { return num; }
        unsigned denominator() const { return denom; }
        bool sign() const { return positive; }
    
        string to_string()
        {
            string str;
            if(! positive)
                str+="-";
            str += to_string(num);
            str+=" / ";
            str+= to_string(denom);
            return str;
        }
};

fraction::fraction(int num, int denom) 
    : num(abs(num)), denom(abs(denom)), positive((num >= 0 && denom >= 0) || (num <= 0 && denom <= 0))
{}

fraction::fraction(unsigned num, unsigned denom, bool positive) 
    : num(num), denom(denom), positive(positive)
{}

fraction fraction::operator *(const fraction& f) const
{
    return fraction(num * f.num, denom * f.denom, 
        (positive && f.positive) || (!positive && !f.positive));
}

fraction fraction::operator*(const int& i) const
{
    return fraction(i * numerator(), denominator());
}

int main()
{
    fraction f1 (2,3);  // 2/3  //fraction f11(2,3,false);
    fraction f2 (-4,5); //- 4 / 5
    
    fraction f3 = f1 * f2;
    cout << f3.to_string() << endl;
    
    fraction f4 = f1 * 2;
    cout << f4.to_string() << endl;
    //fraction f5 = 2 * f1; //impossible with members

    cout << (f1 * f2 * f3).to_string() << endl; //((f1 * f2) * f3)
}
