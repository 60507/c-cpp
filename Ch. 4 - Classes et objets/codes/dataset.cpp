#include <iostream>
#include <initializer_list>

using namespace std;

class DataSet
{
    double _sum; //try without _
    int _count;

    public:
        DataSet() : _sum(0), _count(0) {}

        DataSet(const initializer_list<double>& data) : DataSet()
        {
            update(data);
        }

        void update(const initializer_list<double>& data)
        {
            for(double d : data)
                update(d);
        }

        void update(double d)
        {
            _sum += d;
            _count++;
        }
    
        double sum() const
        {
            return _sum;   
        }
    
        int count() const
        {
            return _count;   
        }

        double mean() const
        {
            return _sum / _count;
        }
};

int main()
{
    DataSet set = {1,2,3};
    cout << set.mean() << endl;
    set.update({4,5,6,7});
    cout << set.mean() << endl;
}
