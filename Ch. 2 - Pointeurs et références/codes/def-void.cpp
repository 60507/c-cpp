#include <iostream>

using namespace std;

struct A
{
    int i;
};

struct B
{
    int i; int j;  
};

int main()
{
    A a {1};
    B b {2, 3};
    
    A* pta = &a;
    B* ptb = &b;
    
    void * ptv = pta; //ok
    //cout << (*ptv).i << endl;    
    cout << (static_cast<A*>(ptv))->i << endl;
    cout << (static_cast<B*>(ptv))->i << endl; //probably wrong
    cout << (static_cast<B*>(ptv))->j << endl; //probably wrong
}
