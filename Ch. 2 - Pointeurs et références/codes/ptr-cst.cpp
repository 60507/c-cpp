#include <iostream>

using namespace std;

int main()
{
    int i = 2;
    const int ci = 3; //int const ci = 3 similaire
    i += 2;
    //ci += 2; //ko

    int * pi = &i;
    cout << pi << " : " << i << endl;
    *pi = 3; 
    cout << pi << " : " << i << endl;

    int * const cpi = &i; //ptr constant    
    //int * const cpi = &ci; //error
    *cpi = 5;
    //cpi++;

    const int * pic = &ci; //ptr d'entier constant    
    //const int * pic = &i; //ok
    //*pic = 4;    
    pic++;

    const int * const cpic = &ci; //ptr cst d'entier cst
    //*cpic = 4;    
    //cpic++;
}
