#include <iostream>

using namespace std;

int main()
{
    int a = 42;
    int b = 43;

     // a and b are both lvalues:
    a = b; // ok
    b = a; // ok
    a = a * b; // ok

    // a * b is an rvalue:
    int c = a * b; // ok, rvalue on right hand side of assignment
    //a * b = 42; // error, rvalue on left hand side of assignment

    //2 is a rvalue
    int d = 2;
    //int & rd = 2; //error
    const int& e = 2; //ok : you are allowed to bind a const lvalue to a rvalue

    //conv    
    int f = 2;
    //long int & rf = e; //ko: cannot bind to non const
    double g = 2.42;
    //int& rg = g; //error
    const int& rgg = g; //ok : you are allowed to bind a const lvalue to a rvalue
}
