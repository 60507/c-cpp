#include <iostream>

using namespace std;

long unsigned sneaky(int array[])
{
    cout << sizeof(array) << " " << sizeof(*array) << endl;
    return sizeof(array) / sizeof(*array);
}

void print_tab(int tab[], int length)
{
    for(unsigned i = 0; i < length; i++)
        cout << tab[i] << " ";
    cout << endl;
}

void copy_tab(int tab1[], int tab2[], int l1)
{
    for(unsigned i = 0; i < l1; i++)
        tab1[i] = tab2[i];
}

int main()
{
    int t1[5] = {1,2,3,4};
    int t2[] = {1,2,3,4,5};
    int * t3 = t2;
    //int t4[]; 
    //int t5[5] = {1,2,3,4,5,6}; 
    int t6[8]; // = {}
    
    for(int i = 0; i < 5; i++)
        cout << t1[i] << " " << t2[i] << " " << t3[i] << " " << t6[i] << " " << tg[i] << endl;
        
    cout << sizeof(t6) << " " << sizeof(*t6) << endl;
    cout << (sizeof(t6) / sizeof(*t6)) << endl;
    cout << sneaky(t6) << endl;   
    
    print_tab(t2, sizeof(t2) / sizeof(*t2));
    
    int i = *(t2 + 3); //4e elem
    cout << i << endl;
    
    int t2d[2][3];
    for(unsigned i = 0; i < 2; i++)
        for(unsigned j = 0; j < 3; j++)
            t2d[i][j] = i * j;
    for(unsigned i = 0; i < 2; i++)
    {
        for(unsigned j = 0; j < 3; j++)
            cout << t2d[i][j] << " ";
        cout << endl;
    }
    cout << endl;
    
    int * it = (int*)t2d; //felony : you shouldn't
    for(unsigned i = 0; i < 2; i++)
    {
        for(unsigned j = 0; j < 3; j++)   
        {
            cout << *it << " ";
            it++;
        }
        cout << endl;
    }
}
