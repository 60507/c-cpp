#include <iostream>

using namespace std;

int main()
{
    int i = 3;
    int * pti = &i;    

    cout << "i = " << i << ", pti (" << pti << ") : " << *pti << endl;
    cout << "Pointer address : " << &pti << " of size " << sizeof(pti) << endl;
    
    i++;
    cout << "i = " << i << ", pti (" << pti << ") : " << *pti << endl;
    cout << "Pointer address : " << &pti << " of size " << sizeof(pti) << endl;

    double d = 2.5;
    double * ptd = &d;    
    //pti = ptd; //Error
    //ptd = pti; //Error
    *pti = *ptd; //ok
    cout << "i = " << i << ", pti (" << pti << ") : " << *pti << endl;
    cout << "Pointer address : " << &pti << " of size " << sizeof(pti) << endl;
    cout << "d = " << d << ", ptd (" << ptd << ") : " << *ptd << endl;
    cout << "Pointer address : " << &ptd << " of size " << sizeof(ptd) << endl;
    
    int * ptn = NULL; //ok, but terrible practice
    int * ptn2 = nullptr; //ok        
    //cout << *ptn2 << endl; //undefined behaviour
    ptn2 = &i;
    cout << *ptn2 << endl;

    void * ptv = pti;
    //cout << *ptv << endl; //error
    cout << *(static_cast<int*>(ptv)) << endl;
}
