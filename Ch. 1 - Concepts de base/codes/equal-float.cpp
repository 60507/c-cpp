#include <iostream>
#include <cmath>

using namespace std;

const double EPSILON = 10E-9;

bool equal_float(double d1, double d2)
{
    return abs(d1 - d2) < EPSILON;
}

int main()
{
    double a = 2.0;
    double b = 3.0;
    
    cout << (a + b == a + b) << endl;
    cout << (a + b == b + a) << endl;
}
