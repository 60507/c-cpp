#include <iostream>
#include <string>
#include <stdexcept>

void synch()
{
    int n, p;
    std::cout << "Donnez une valeur pour n" << std::endl;//1 2
    std::cin >> n;
    std::cout << "Vous avez tapé " << n << std::endl;
    std::cout << "Donnez une valeur pour p" << std::endl;
    std::cin >> p;
    std::cout << "Vous avez tapé " << p << std::endl;
}

void block()
{
    int n = 12;
    char c = 'a';
    std::cout << "Donnez un entier et un caractère" << std::endl;//x 25
    std::cin >> n >> c;
    std::cout << "Vous avez tapé " << n << " et " << c << std::endl;
    std::cout << "Donnez un caractère" << std::endl;
    std::cin >> c;
    std::cout << "Vous avez tapé " << c << std::endl;
}

void loop()
{
    //3
    //à
    int n = -1;
    do
    {
        std::cout << "Tapez un nombre entier " << std::endl;
        std::cin >> n;
        std::cout << "Son carré est " << n*n << std::endl;
    }
    while(!n);    
}

void good()
{
    std::cout << "Tapez un entier" << std::endl;
        
    std::string line;
    std::getline(std::cin, line);
    
    try
    {
        int i = std::stoi(line);
        std::cout << "Vous avez tapé " << i << std::endl;
    }
    catch(std::invalid_argument& ex)
    {
        std::cout << "Pas un entier" << std::endl;
    }
}

int main()
{
    //synch();
    //block();
    //loop();
    good();
}
