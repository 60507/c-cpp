#include <iostream>

using namespace std;

void f(int i) {}
void f(double d) {}

void g(double) {}

int main()
{
    //integral promotion : char -> int, bool -> int, short -> int
    //numerical conversion : anything else
    
    int i1 = 2;  //no conv/prom : 2 is a int literal
    int i2 = 2L; //conv : long -> int
    long i3 = 2; //conv : int -> long
    double d1 = 2; //conv : int -> double
    int i4 = 2.0; //conv : double -> int
    
    //pbm if multiple convs availlable  
    //f(2L); //error - 2 availlable convs : long -> int, long -> double
    f(true); //ok, integral promotion availlable (and it's better than a conv)
    
    g(true); //ok, promotion bool -> int, followed by conv int -> double
}
