#include <iostream>

struct A
{
	int i; //size 4
};

struct B
{
	char c; //size 1
};

struct C
{
	int i; //size 4
	char c; //size 1
	//3 bytes padding
}; //size 8

int main()
{
	std::cout << sizeof(A) << " " << sizeof(B) << " " << sizeof(C) << std::endl;
}
