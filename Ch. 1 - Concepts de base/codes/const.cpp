struct A
{
    void f() const {}
    void g() {}
};

int main()
{
    int i = 2;
    const int ci = i;
    i++; //ok
    //ci++; //ko
    
    A a;
    const A ca;    
    
    a.f(); //ok
    a.g(); //ok
    ca.f(); //ok
    //ca.g(); //ko
}
