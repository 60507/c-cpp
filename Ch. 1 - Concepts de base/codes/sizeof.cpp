#include <iostream>

int main()
{
    std::cout << "Size of char             : " << sizeof(char) << std::endl;
    std::cout << "Size of short            : " << sizeof(short) << std::endl;
    std::cout << "Size of int              : " << sizeof(int) << std::endl;
    std::cout << "Size of long      int    : " << sizeof(long int) << std::endl;
    std::cout << "Size of long long int    : " << sizeof(long long int) << std::endl;
    std::cout << "Size of float            : " << sizeof(float) << std::endl;
    std::cout << "Size of double           : " << sizeof(double) << std::endl;
    std::cout << "Size of long double      : " << sizeof(long double) << std::endl;
    std::cout << "Size of adress           : " << sizeof(int*) << std::endl;
}
