#include <iostream>
#include <string>

int main()
{
	std::string s1 = "Hello ";
	std::string s2 = "World";

	for(long i = 0; i < s1.length(); i++)
		std::cout << s1[i] << std::endl;
	for(long i = 0; i < s2.length(); i++)
		std::cout << s2[i] << std::endl;
	std::cout << std::endl;

	std::string s3 = s1 + s2;
	std::cout << s3 << std::endl;	
	s1 += s2;
	std::cout << s1 << std::endl << std::endl;

	std::string s4;
    std::cout << "Length = " << s4.length() << ", capacity = " << s4.capacity() << std::endl;
	for(int i = 0; i < 200; i++)
	{
		s4 += 'a';
		std::cout << "Length = " << s4.length() << ", capacity = " << s4.capacity() << std::endl;		
	}
}
