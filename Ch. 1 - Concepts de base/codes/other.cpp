#include <iostream>
#include <cstdint>

int main()
{    
    char c = 'A';
    std::cout << c << std::endl;
    char d = 'é'; //unsupported
    std::cout << d << std::endl;
    
    int8_t i8 = 0;
    int16_t i16 = 0;
    int32_t i32 = 0;
    int64_t i64 = 0;
    std::cout << sizeof(i8) << " " << sizeof(i16) << " " << sizeof(i32) << " " << sizeof(i64) << std::endl;

    int n = 2;
    int m = n;
    int & rn = n;
    int * pn = &n;    

    n++;
    std::cout << n << " " << m << " " << rn << " " << *pn << std::endl;
}
