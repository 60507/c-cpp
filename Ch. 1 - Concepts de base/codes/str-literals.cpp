#include <iostream>

int main()
{    
    char * s1 = "Hello"; //warning : const -> nconst    
    std::cout << s1 << std::endl;
    //s1[0] = 'h'; //undefined behaviour
    //std::cout << s1 << std::endl;
    
    char s2[] = "Hello"; //not the same as s1
    std::cout << s2 << std::endl;
    s2[0] = 'h';
    std::cout << s2 << std::endl;

    //hello : length = 5
    //char s3[5] = "Hello"; //ko : missing \0
    char s4[6] = "Hello"; //ok
    char s5[20] = "Hello"; //filled with zero bytes
    
    //std::cout << s3 << "|" << std::endl;
    std::cout << s4 << "|" << std::endl;
    std::cout << s5 << "|" << std::endl;
}