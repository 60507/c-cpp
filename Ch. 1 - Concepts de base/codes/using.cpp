#include <iostream>
#include <utility>
#include <vector>

using namespace std;

struct point {};

using two_points = std::pair<point,point>;

template<class T> 
using ptr = T*;

using It_int = decltype(std::vector<int>().begin());

int main()
{
    point p1;
    point p2;
    two_points two_pts(p1, p2);
        
    ptr<point> pt = &p1;
    cout << &p1 << endl;
    cout << pt << endl;
    
    vector<int> v = {1, 2, 3, 4, 5};
    It_int it = v.begin();
    cout << *it << endl;
}
