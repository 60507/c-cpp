#include <iostream>

using namespace std;

int k = 3; 

int k_plus_one() { k++; return k; }
int k_plus_two() { k+=2; return k; }

void f(int i, int j)
{
    cout << (i*2) + j << endl;
}

int main()
{            
    f(k_plus_one(), k_plus_two()); //4 * 2 + 6 = 14 if +1 before +2
                                   //5 * 2 + 6 = 16 if +2 before +1
}
