#include <iostream>

int main()
{
    bool b = true;
    if(b)
      std::cout << "true" << std::endl;  
    else
      std::cout << "false" << std::endl;

    if(-1)
      std::cout << "-1 true" << std::endl;
    else
      std::cout << "-1 false" << std::endl;
    
    if(0)
      std::cout << "0 true" << std::endl;
    else
      std::cout << "0 false" << std::endl;
    
    if(1)
      std::cout << "1 true" << std::endl;
    else
      std::cout << "1 false" << std::endl;
}
