#include <iostream>
#include <vector>

using namespace std;


template<int i, template<class> class Collection, class Element>
const Element& get(Collection<Element>& c)
{
    return c[i];
}

template<class E>
class Array
{
    E* a;
    int _size;

    public:
        Array(std::initializer_list<E> l) : a(new E[l.size()]), _size(l.size())
        {
            std::copy(l.begin(), l.end(), a);
        }    

        ~Array()
        {
            delete[] a;
        }

        Array(const Array<E>& array) = delete;
        Array& operator=(const Array<E> array) = delete;
        //I probably should override move constructor

        E& operator[](unsigned i)
        {
            return a[i];
        }
    
        const E& operator[](unsigned i) const
        {
            return a[i];
        }
};

int main()
{
    Array<int> a = {1,2,3,4,5};
    cout << (get<2>(a)) << endl;
}
