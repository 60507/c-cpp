#include <iostream>

using namespace std;

struct A 
{
    void a() const { cout << "A" << endl; }
};

struct B
{
    int i;
    
    void b() const { cout << "B" << endl; }
};

template<class T>
constexpr bool is_A()
{
    return sizeof(T) == sizeof(A);   
}

template<class T>
void f(const T& t)
{
   if constexpr(is_A<T>())
       t.a();
    else
        t.b();
};

int main()
{
    A a;
    f(a);
    
    B b;
    f(b);
}
