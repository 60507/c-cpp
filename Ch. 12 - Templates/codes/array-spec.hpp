#include <iostream>
#include <stdexcept>
#include <iostream>
#include <initializer_list>
#include <iterator>
#include <algorithm>

template<class T, int N> class Array
{
    T t[N];
    
    public:
        Array() = default;
    
        Array(const std::initializer_list<T>& l) : Array()
        {
            if(N != l.size())
                throw std::out_of_range("Non matching sizes");
            std::copy(l.begin(), l.end(), t);
        }

        T& operator[](unsigned i) { return t[i]; } 
        const T& operator[](unsigned i) const { return t[i]; }
    
        auto begin() noexcept { return std::begin(t); }
        const auto begin() const noexcept { return std::begin(t); }
        auto end() noexcept { return std::end(t); }
        const auto end() const noexcept { return std::end(t); }
    
        void print() const
        {
            std::for_each(std::begin(t), std::end(t), [](const T& t) { std::cout << t << " "; });   
            std::cout << std::endl;
        }
};

//specialization where T is an address
//deferences on print
template<class T, int N> class Array<T*,N> 
{
    T* t[N]; //array of N addresses of T
    
    public:
        Array() = default;
    
        Array(const std::initializer_list<T*>& l) : Array()
        {
            if(N != l.size())
                throw std::out_of_range("Non matching sizes");
            std::copy(l.begin(), l.end(), t);
        }

        T*& operator[](unsigned i) { return t[i]; } 
        const T*& operator[](unsigned i) const { return t[i]; }
        
        auto begin() noexcept { return std::begin(t); }
        const auto begin() const noexcept { return std::begin(t); }
        auto end() noexcept { return std::end(t); }
        const auto end() const noexcept { return std::end(t); }
    
        void print() const
        {
            std::for_each(std::begin(t), std::end(t), [](const T* const & t) { std::cout << *t << " "; }); 
            std::cout << std::endl;
        }
};

//specialization for 2D arrays
//prints as a 2D array
template<class T, int N, int M> class Array<Array<T,M>, N> //N rows, M columns
{
    Array<T,M> t[N];
    
    public:
        Array() = default;
        Array(const std::initializer_list<Array<T,M>>& l) : Array()
        {
            if(N != l.size())
                throw std::out_of_range("Non matching sizes");

            std::copy(l.begin(), l.end(), t);
        }
    
        Array<T,M>& operator[](unsigned i) { return t[i]; }
        const Array<T,M>& operator[](unsigned i) const { return t[i]; }
    
        auto begin() noexcept { return std::begin(t); }
        const auto begin() const noexcept { return std::begin(t); }
        auto end() noexcept { return std::end(t); }
        const auto end() const noexcept { return std::end(t); }
    
        void print() const
        {
            std::for_each(std::begin(t), std::end(t), [](const auto& line)
            {
                std::for_each(std::begin(line), std::end(line), [](const T& t){ std::cout << t << " "; });
                std::cout << std::endl;
            });
            std::cout << std::endl;
        }
};