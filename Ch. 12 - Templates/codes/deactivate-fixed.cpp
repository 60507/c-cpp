#include <iostream>

using namespace std;

template<class T>
struct A
{
    A(T t) {}
    
    template<class U=T> requires (! std::integral<U>)
    A(int i) {}
};

int main()
{
    A<double> a (2.0); //ok
    A<int> b(2); //ok
}
