#include "stdio.h"

void print_array(int array[], unsigned size)
{
    for(unsigned i = 0; i < size; i++)
        printf("%d ", array[i]);
    printf("\n");
}

long unsigned compute_size(int array[])
{
    return sizeof(array) / sizeof(*array);
}

int main()
{
    int array[] = {1,2,3,4,5,6,7,8};
    printf("%lu\n", sizeof(array) / sizeof(*array)); //32 / 4 (if sizeof(int)=4)
    printf("%lu\n", compute_size(array));            //8 / 4 (if x64 and sizeof(int)=4)
    
    print_array(array, 8);
}
