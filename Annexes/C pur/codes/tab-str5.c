#include "stdio.h"
#include "string.h"

int main()
{
    char * s1 = "Hello ";
    char * s2 = "world!";
    char buffer[strlen(s1) + strlen(s2) + 1];
    strcpy(buffer, s1);
    strcat(buffer, s2);
    printf("%s\n", buffer);
}
