#include <stdio.h>
#include <math.h>

#define PI atan(1) * 4
#define area(r) (PI * (r) * (r))

int main()
{
    double r = 1;
    printf("area of circle of radius 1: %f\n", area(r));
    
    //area(r) is expanded as (atan(1) * 4 * (1) * (1))
}
