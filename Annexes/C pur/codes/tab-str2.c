#include "stdio.h"
#include "string.h"
#include "stdlib.h"

int main()
{
	char s1[6] = "01234"; //6, not 5
	char s2[20] = "012345";
	printf("%zu\n", strlen(s1));
	printf("%zu\n", strlen(s2));    
	
	char * s3 = "01234";	
	char * s4 = malloc( sizeof(char) * 20);
    //memset(s4, 0, 20); //forces zeros in s4
	
	printf("%zu\n", strlen(s3));
	printf("%zu\n", strlen(s4));
	
	for(int i = 0; i < strlen(s3); i++)
		s4[i] = s3[i];
		
	printf("%zu\n", strlen(s3));
	printf("%zu\n", strlen(s4));
	
	//"Coucou !"[0] = 'c';
}