#include "stdio.h"
#include "string.h"

int main()
{
    char s1[] = "Hello ";
    //char s1[20] = "Hello "; //ok
    char s2[] = "world!";
    strcat(s1, s2);
    printf("%s\n", s1);
}
