#include "code42-odd.h"
#include "code42-even.hpp"

bool is_odd(int n)
{
    return ! is_even(n);   
}