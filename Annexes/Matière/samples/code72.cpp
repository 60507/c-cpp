//NICE!
#include <iostream>

template<class T, class U>
T cast(const U& u)
{
    return static_cast<T>(u);   
}

int main()
{
    int i = cast(2.2);
    std::cout << i << std::endl;
}

//want 2
//has comp. error
