#include <iostream>
#include <vector>

template<template<class, class ...> class Container, class T, class... Whatever>
void increment(Container<T, Whatever...>& c)
{
    for(auto i : c)
        i++;
}

int main()
{
    std::vector<int> v = {1,2,3};
    increment(v);
    for(auto i : v)
        std::cout << i << " ";
    std::cout << endl;
}

//want 2 3 4
//has 1 2 3
