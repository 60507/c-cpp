#include <iostream>
#include <vector>

using namespace std;

template<int I, class Container>
auto& get(Container& c)
{
    return c[I];
}

int main()
{
    vector<int> v = {0, 1, 2, 3, 4};
    cout << get(v) << endl;
}

//want 2
//has comp. error