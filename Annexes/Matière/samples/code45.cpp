#include <iostream>

using namespace std;

struct Ball {};

struct Player
{
    Player* target;
    Player(Player* p = nullptr) : target(p) {}
    
    void throw_ball(Ball b)
    {
        try
        {
            throw b;   
        }
        catch(...)
        {
            cout << "I caught the ball and I'm rethrowing it !" << endl;
            target->throw_ball(b);
        }
    }
};

int main()
{
    Player father; 
    Player son(&father);
    father.target = &son;
    
    Ball b;
    father.throw_ball(b);
}

//want I caught the ball and I'm rethrowing it
//has lots of printed lines and eventually a seg fault
