#include <iostream>

auto f(int i);

int main()
{
    std::cout << f(0) << std::endl;
    std::cout << f(1) << std::endl;
}

auto f(int i)
{
    if(i == 0)
        return 1;
    else
        return 0;
}

//want 1 0
//have comp. error