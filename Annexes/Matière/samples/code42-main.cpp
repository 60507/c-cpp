#include <iostream>
#include "code42-even.hpp"
#include "code42-odd.h"

using namespace std;

int main()
{
    cout << is_even(42) << endl;
    cout << is_odd(42) << endl;
}

//want 1 0
//have ld error