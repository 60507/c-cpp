#include <iostream>

using namespace std;

struct A {}
struct B : A{
    ~B() { cout << "-B"; }
};

int main()
{
    A * a = new B;
    delete a;
    cout << "Hello" << endl;
}

//want -B Hello
//has Hello