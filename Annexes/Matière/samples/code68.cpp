#include <iostream>

struct A1
{
    A1() { std::cout << "+A1" << std::endl; }  
    ~A1() { std::cout << "-A1" << std::endl; }  
};

struct A2
{
    A2() { std::cout << "+A2" << std::endl; }  
    ~A2() { std::cout << "-A2" << std::endl; }  
};

struct B : A1, A2
{
    B() { std::cout << "+B" << std::endl; }  
    ~B() { std::cout << "-B" << std::endl; }  
};

int main()
{
    B b;
}

//want +A2 +A1 +B -B -A1 -A2
//has  +A1 +A2 +B -B -A2 -A1
