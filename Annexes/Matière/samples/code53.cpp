#include <iostream>

using namespace std;

struct A {};

struct B
{
    A a;
    
    bool operator==(const B&) const = default;
};

int main()
{
    B b;
    cout << (b == b) << endl;
}

//want 1
//has comp. error
