#include <iostream>
#include <type_traits>

template<class T>
concept Integral = std::is_integral_v<T>;

template<Integral T>
concept SignedIntegral = std::is_signed_v<T>; 

template<SignedIntegral I>
void increment(I& i)
{
    i++;   
}

int main()
{
    int i = 0;
    increment(i);
    std::cout << i << std::endl;
}

//want 3
//has comp. error