#include "code20.h"

auto is_even(auto n)
{
    return n % 2 == 0;
}