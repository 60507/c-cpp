import os
import re
import subprocess

def shift_codes(start, end, shift):
    files = [f for f in os.listdir(".") if os.path.isfile(f)]
    files.sort()
    if shift > 0:
        files.reverse()
    for f in files:
        number = re.search(r'\d+', f)
        if number:
            number = number.group()
            if start <= int(number) <= end:
                parts = f.split(number)                
                newf = parts[0] + str(int(number) + shift) + parts[1]
                print(f, "->", newf)
                subprocess.call(["git", "mv", f, newf])