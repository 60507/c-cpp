#include <iostream>
#include "code32-a.h"
#include "code32-b.h"

int main()
{
    A a {1}; B b { 2, &a }; a._b = &b;
    std::cout << a.i() << " " << b.i() << std::endl;
}

//want 1 2
//has comp. error