#include <iostream>
#include <list>

using namespace std;

int main()
{
    list<int> l = {1, 2, 3, 4, 5};
    for(int i = 0; i < l.size(); i++)
    {
        cout << l.back() << " " << endl; //prints last element
        l.pop_back(); //removes last element
    }
}

//Want 5 4 3 2 1
//has 5 4 3