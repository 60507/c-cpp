#include <iostream>

using namespace std;

void print(void* pt)
{
    cout << static_cast<int>(*pt) << endl;   
}

int main()
{
    int i = 2;
    print(&i);
}

//want 2
//have comp. error