#include <iostream>
#include <cstring>

using namespace std;

const char* str = "Hello World";
constexpr unsigned len = strlen(str);

int main()
{
    cout << len << endl;
}

//want 11
//have comp. error
