#include <iostream>

using namespace std;

const int& f()
{
    const int i = 2;
    return i;
}

int main()
{
    cout << f() << endl;
}

//want 2
//seg fault