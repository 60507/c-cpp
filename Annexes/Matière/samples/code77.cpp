#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

using namespace std;

template<template<class, class ...> class Container, class T, class ... Whatever>
ostream& operator<<(ostream& out, const Container<T,Whatever...> & c)
{
    for_each(c.begin(), c.end(), [](const T& t){ cout << t << " "; });
    return out;
}

int main()
{
    std::vector<int> v = {0, 1, 2, 3, 4};
    cout << v << endl;
    string s = "Hello";
    cout << s << endl;
}

//want 0 1 2 3 4
//has comp. error
