#ifndef ARRAYOBSERVER_H
#define ARRAYOBSERVER_H

template<class T> class ArrayEvent;
template<class T> class SwapEvent;
template<class T> class SizeEvent;

/**
 * This class models an abstract observer for arrays.
 * <p>
 * This observer can monitor operations performed on an array,
 * such as getting, setting and swapping elements, as well as
 * requesting the size of the observed array.
 * <p>
 * Everytime such en operation happens, an event is fired, and
 * associated actions are performed according the the type and
 * content of the fired event.
 * <p>
 * Specificities about the content and sructure of these events
 * can be found in the subject classes, that is, in the classes
 * which are observed.
 * @brief This class models an abstract observer for arrays.
 * @tparam T the type of the datas of the oberved array
 * @see ArraySubject<T>
 */
template<class T>
class ArrayObserver
{
    public:
        /**
         * This member function is transparently called by
         * the subject, that is, the observed array, every
         * single time an element is set.
         * <p>
         * The event is assumed to store relevant information
         * regarding the element that has been set as well as
         * the array in which it has been set.
         * @brief This member function is called each time an
         * element is set.
         * @param e the event associated with the set operation
         */
        virtual void elementSet(ArrayEvent<T> e) = 0;

        /**
         * This member function is transparently called by
         * the subject, that is, the observed array, every
         * single time an element is obtained.
         * <p>
         * The event is assumed to store relevant information
         * regarding the element that has been obtained as well
         * as the array in which it has been obtained.
         * @brief This member function is called each time an
         * element is obtained.
         * @param e the event associated with the get operation
         */
        virtual void elementGot(ArrayEvent<T> e) = 0;

        /**
         * This member function is transparently called by
         * the subject, that is, the observed array, every
         * single time two elements are swapped.
         * <p>
         * The event is assumed to store relevant information
         * regarding the elements that have been swapped as well
         * as the array in which they have been swapped.
         * @brief This member function is called each time two
         * elements are swapped.
         * @param e the event associated with the swap operation
         */
        virtual void elementsSwapped(SwapEvent<T> e) = 0;

        /**
         * This member function is transparently called by
         * the subject, that is, the observed array, every
         * single time its size is requested.
         * <p>
         * The event is assumed to store relevant information
         * regarding the array whose size has been requested..
         * @brief This member function is called each time the
         * size of an array is requested.
         * @param e the event associated with the size operation
         */
        virtual void sizeRequested(SizeEvent<T> e) = 0;
};

#endif // ARRAYOBSERVER_H
