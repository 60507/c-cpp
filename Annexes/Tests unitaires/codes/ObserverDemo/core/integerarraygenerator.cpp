#include "integerarraygenerator.h"

IntegerArrayGenerator::IntegerArrayGenerator(int min, int max)
    : min(max > min ? min : max), max(max > min ? max : min)
{

}

Array<int>* IntegerArrayGenerator::ascending()
{
    Array<int> * array = new Array<int>(max - min + 1);
    for(int i = 0; i < array->size(); i++)
        array->set(i, min + i);
    return array;
}

Array<int> *IntegerArrayGenerator::descending()
{
    Array<int> * array = new Array<int>(max - min + 1);
    for(int i = 0; i < array->size(); i++)
        array->set(i, max - i);
    return array;
}

Array<int> *IntegerArrayGenerator::random()
{
    Array<int> * array = ascending();
    for(int i = array->size() - 1; i >= 0; i--)
        array->swap(i, std::rand() % (i + 1));

    return array;
}

