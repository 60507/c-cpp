TEMPLATE = subdirs

SUBDIRS += \
    core \
    tests \
    controllers \    
    console \    

OTHER_FILES += \
    defaults.pri
