#include "array.hpp"
#include "insertionsort.hpp"
#include "permutationsort.hpp"
#include "bubblesort.hpp"
#include "heapsort.hpp"

#include "arraysubject.hpp"
#include "printingobserver.hpp"

#include <iostream>

using namespace std;

void print(Array<int> &a);
void makeSomeResets(Array<int> &a);
void makeSomeSwaps(Array<int> &a);
void consoleDemo();
void sortersDemo();

int main()
{    
    consoleDemo();
    //sortersDemo();
}

void consoleDemo()
{
    ArraySubject<int> subject = {1,2,3,4,5};
    PrintingObserver<int> obs;
    subject.registerObserver(obs);

    print(subject);
    cout << endl;

    makeSomeResets(subject);
    cout << endl;

    makeSomeSwaps(subject);
}

void sortersDemo()
{
    ArraySubject<int> subject = {2,3,1,5,4};
    PrintingObserver<int> obs;
    subject.registerObserver(obs);

    //InsertionSort<int> sort(subject);
    //PermutationSort<int> sort(subject);
    //BubbleSort<int> sort(subject);
    HeapSort<int> sort(subject);
    sort.sort();

    cout << endl;
    print(subject);
}

void print(Array<int>& a)
{
    for(int i = 0; i < a.size(); i++)
        cout << "a[" << i << "] = " << a[i] << endl;
}

void makeSomeResets(Array<int> & a)
{
    for(int i = 0; i < a.size(); i += 2)
        a.set(i, 0);
}

void makeSomeSwaps(Array<int> & a)
{
    for(int i = 0; i < a.size() - 1; i += 3)
        a.swap(i, i + 1);
}
