#include <iostream>

using namespace std;

/*
auto gcd(auto a, auto b)
{
    if(b == 0)
        return a;
    else
        return gcd(b, a % b);
}
*/

/*
auto gcd(auto a, auto b) //error
{
    if(b != 0)
        return gcd(b, a % b);
    else
        return a;
}
*/

auto gcd(auto a, auto b) -> decltype(a)
{
    if(b != 0)
        return gcd(b, a % b);
    else
        return a;
}

/*
auto gcd(auto a, auto b) //also error
{
    return b == 0 ? a : gcd(b, a % b);
}
*/

/*
auto gcd(auto a, auto b) //still an error
{
    return b != 0 ? gcd(b, a % b) : a;
}
*/

int main()
{
    cout << gcd(48, 64) << endl;
}
