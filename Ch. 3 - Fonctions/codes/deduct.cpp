#include <iostream>

using namespace std;

auto fwd(auto f, int i) //-> decltype(f(i))
{
    cout << "Type of fct: " << typeid(f).name() << endl;
    return f(i);   
}

void print(int i)
{
    cout << i << endl;   
}

int plus_one(int i)
{
    return i + 1;   
}

int main()
{
    fwd(print, 2);
    print(fwd(plus_one, 2));
}
