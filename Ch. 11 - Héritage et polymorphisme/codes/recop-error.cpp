#include <iostream>

using namespace std;

class point
{
    protected:
        int _x, _y;
    
    public:
        point(int a, int b) : _x(a), _y(b) {} //try to remove cstr-def
        int x() const { return _x; }
        int y() const { return _y; }
        
};

ostream& operator <<(ostream& out, const point& p)
{
    return (out << "( " << p.x() << " , " << p.y() << " )");
} 

class pointcol : public point
{
    short r, g, b;
    
    public:
        pointcol(int x = 0, int y = 0, int r = 0, int g = 0, int b = 0) : point(x,y), r(r), g(g), b(b) {}        

        pointcol(const pointcol &p) : r(p.r), g(p.g), b(p.g) //no call to point::point(int,int)
        {
            cout << "+r pointcol" << endl;
        }
};

void f(pointcol p)
{
    cout << "f " << p << endl;
}

int main()
{
    pointcol a(1,2,255,128,128);
    f(a);
}
