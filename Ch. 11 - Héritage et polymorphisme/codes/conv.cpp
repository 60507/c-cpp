#include <iostream>

using namespace std;

class point
{
    protected:
        int _x, _y;
    
    public:
        point(int x = 0, int y = 0) : _x(x), _y(y) {}
        
        int x() const { return _x; }
        int y() const { return _y; }
};

ostream& operator<<(ostream& out, const point& p)
{
    return (out << "( " << p.x() << " , " << p.y() << " )");
}

class pointcol : public point
{
    short _r, _g, _b;
    
    public:
        pointcol(int x = 0, int y = 0, int r = 255, int g = 255, int b = 255) 
            : point(x,y), _r(r), _g(g), _b(b) {}
            
        short r() const { return _r; }
        short g() const { return _g; }
        short b() const { return _b; }
};

ostream& operator<<(ostream& out, const pointcol& p)
{
    return (out << static_cast<const point&>(p)
                << " - color " << p.r() << " " << p.g() << " " << p.b());
}

int main()
{    
    point p (1,2);
    pointcol pc (3,4,128,255,255);    
    cout << p << endl;   //perfect match static call
    cout << pc << endl;  //perfect match static call    
    
    p = pc; //pp truncated: p is "really" a point
    //pc = p; // ko
    //pc = static_cast<pointcol>(p); //ko    
    cout << p << endl;   //perfect match static call
    cout << pc << endl;  //perfect match static call
    cout << endl;

    p  = point(1,2); point& rp = p;
    pc = pointcol(3,4,128,255,255); pointcol& rpc = pc;
    rp = rpc; //no truncation
    pointcol& rpc2 = static_cast<pointcol&>(rp);// ok, but incoherent result for r, g, b    
    cout << rp << endl;   //perfect match static call
    cout << rpc << endl;  //perfect match static call
    cout << rpc2 << endl; //perfect match static call
    cout << endl;
        
    point * ptp = &p;
    pointcol * ptpc = &pc;
    ptp = ptpc; //no truncation
    cout << (*ptp) << endl;  //perfect match static call
    cout << (*ptpc) << endl; //perfect match static call
        
    ptp = &p;
    //ptpc = ptp; //ko    
    ptpc = static_cast<pointcol*>(ptp);//ok, but seg fault    
    cout << (*ptp) << endl;  //perfect match static call
    cout << (*ptpc) << endl; //perfect match static call

    pointcol * converted = dynamic_cast<pointcol*>(ptp)
    if(converted != nullptr)
        cout << (*converted) << endl;     //perfect match static call
    else
        cout << "You cannot convert this point to a pointcol" << endl;
}


