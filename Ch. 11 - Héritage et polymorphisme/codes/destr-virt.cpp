#include <iostream>

using namespace std;

struct Mere
{
    ~Mere() { cout << "-M" << endl; }
    virtual void print() { cout << "Mère " << endl; }    
};

struct Fille : Mere
{
    ~Fille() { cout << "-F" << endl; }
    void print() override { cout << "Fille " << endl; }    
};

void print_indep(Mere* m) { m->print(); }

int main() //try to compile with -Wall
{
    Fille f;
    Mere * m = &f;
    print_indep(m);
    
    Mere * mn = new Fille;
    delete mn;
}
