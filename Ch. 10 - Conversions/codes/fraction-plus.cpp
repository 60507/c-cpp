#include <iostream>

using namespace std;

class fraction
{
    unsigned _num, _denom;
    bool _positive;

    public:
        fraction(int num = 0, int denom = 1) : _num(abs(num)), _denom(abs(denom)), _positive((num > 0 && denom > 0) || (num < 0 && denom < 0))
        {}        

        unsigned num() const { return _num; }
        unsigned denom() const { return _denom; }
        bool positive() const { return _positive; }
    
        operator double() const
        {
            return _positive ? (double)_num / _denom : -((double)_num / _denom);
        }        
};

ostream& operator<<(ostream &out, const fraction &p)
{
    if(! p.positive())
        out << "-";

    if(p.num() == 0)
        out << "0";
    else if(p.denom() == 1)
        out << p.num();
    else
        out << p.num() << " / " << p.denom();

    return out;
}

int main()
{
    fraction f = (-1,2);
    cout << f << endl;

    f = 12;
    cout << f << endl;
}
