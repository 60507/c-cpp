#include <iostream>
 
struct B 
{
    virtual void f() {};  // must be polymorphic to use runtime-checked dynamic_cast
};
struct D : B {}; //D inherits from A
 
int main()
{
    D d1;
    B& rb1 = d1; //upcast    
    try
    {
        D& rd = dynamic_cast<D&>(rb1); // downcast
        std::cout << "Downcast sucessful" << std::endl;
    }
    catch(const std::bad_cast& e)
    {
        std::cout << "wrong downcast" << std::endl;
    }
    
    B b2;
    B& rb2 = b2;
    try
    {
        D& rd = dynamic_cast<D&>(rb2); // downcast
        std::cout << "Downcast sucessful" << std::endl;
    }
    catch(const std::bad_cast& e)
    {
        std::cout << "wrong downcast" << std::endl;
    }    
 
    B b3;
    B* ptb3 = &b3;
    D* ptd3 = static_cast<D*>(ptb3); //ok but D component is invalid
    if(D* dd = dynamic_cast<D*>(ptb3))
        std::cout << "downcast successful" << std::endl;
    else
        std::cout << "wrong downcast" << std::endl;
 
    D d4;
    B* b4 = &d4;
    if(D* dd = dynamic_cast<D*>(b4))
        std::cout << "downcast successful" << std::endl;
    else
        std::cout << "wrong downcast" << std::endl;
}
