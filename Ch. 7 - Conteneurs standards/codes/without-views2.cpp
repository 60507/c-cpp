#include <algorithm>
#include <vector>
#include <iostream>

using namespace std;

int main() 
{
    const string txt { "    Hello World" };
    cout << txt << endl;

    auto firstNonSpace = find_if_not(txt.begin(), txt.end(), ::isspace);
    string tmp(firstNonSpace, txt.end());
    transform(tmp.begin(), tmp.end(), tmp.begin(), ::toupper);
    
    cout << tmp << endl;
}

//source: https://www.cppstories.com/2022/ranges-composition/
