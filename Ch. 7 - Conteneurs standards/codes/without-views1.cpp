#include <algorithm>
#include <vector>
#include <iostream>

using namespace std;

int main() 
{
    const vector numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    auto even = [](int i) { return 0 == i % 2; };

    vector<int> tmp;    
    copy_if(begin(numbers), end(numbers), back_inserter(tmp), even);
    vector<int> tmp2(begin(tmp) + 1, end(tmp));
    
    for (auto iter = rbegin(tmp2); iter!=rend(tmp2); ++iter)
        cout << *iter << ' ';        
    cout << endl;
}

//source: https://www.cppstories.com/2022/ranges-composition/
