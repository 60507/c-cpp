#include <iostream>
#include <ranges>
#include <vector>

using namespace std;

void print(const ranges::range auto& container)
{
    for(const auto& e : container)
        cout << e << " ";
    cout << endl;
}

struct A {};

int main()
{
    vector<int> v = {1,2,3,4,5};
    print(v);
    
    string s = "Hello";
    print(s);
    
    //A a;
    //print(a);
}
