#include <string>
#include <iomanip>
#include <iostream>
#include <ranges>

using namespace std;

int main() 
{
    const string txt { "    Hello World" };
    cout << txt << endl;

    auto conv = txt | views::drop_while(::isspace) | views::transform(::toupper);

    string tmp(conv.begin(), conv.end());
    cout << tmp << endl;
}

//source: https://www.cppstories.com/2022/ranges-composition/