#include <algorithm>
#include <vector>
#include <iostream>
#include <ranges>

using namespace std;

int main() 
{
    const vector numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    auto even = [](int i) { return 0 == i % 2; };
 
    ranges::reverse_view rv{ 
        ranges::drop_view { 
            ranges::filter_view{ numbers, even }, 1 
        }
    };
    for (auto& i : rv)
        cout << i << ' ';
    cout << endl;
}

//source: https://www.cppstories.com/2022/ranges-composition/