#include <iostream>
#include <vector>
#include <memory>

using namespace std;

class A
{
    int i;

    public:
        A(int i = 0) : i(i) { cout << "Building " << this << endl; }
        A(const A& a) : i(a.i) { cout << "Copying " << &a << " into " << this << endl; }
        ~A() { cout << "Destroying " << this << endl; }
};

void f(vector<A> v) { cout << "Entering f" << endl; }

int main()
{    
    vector<A> v(3);
    cout << endl;

    vector<A> w(v);
    cout << endl;    

    f(v);//passed by value
    cout << endl;

    vector<A*> y;
    for(int i = 0; i < 4; i++)
        y.push_back(new A(i));
    cout << endl;

    vector<shared_ptr<A>> yy;
    for(int i = 0; i < 4; i++)
        yy.push_back(make_shared<A>(i));

    cout << endl;    

    //memory leak here            
}
