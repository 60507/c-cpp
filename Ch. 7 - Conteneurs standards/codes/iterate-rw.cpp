#include <iostream>
#include <list>

using namespace std;

int main()
{
    list<int> l = {1,2,3,4,5,6,7,8,9,10};
    
    for(unsigned i = 0; i < l.size(); i++)
    {
        cout << *l.rbegin() << endl;
        l.pop_back();
    }
    
    list<int> li;
    list<double> ld;
    li = ld;
    ld = li;
}
