#include <iostream>

using namespace std;

struct resource
{
    bool locked;
    
    resource() : locked(false) {}    

    bool acquire() 
    {
        if(locked)
            return false;
        else
        {
            locked = true;

            cout << "resource " << this << " locked" << endl;

            return true;
        }    
    }
    
    bool liberate() 
    {
        if(! locked)
            return false;
        else
        {
            locked = false;

            cout << "resource " << this << " liberated" << endl;

            return true;            
        }        
    }
};

resource r1;
resource r2;

class resource_ptr
{
    private:
        resource* ptr;
        bool acquired;

    public:
        resource_ptr(resource& r) : ptr(&r), acquired(r.acquire()) {}

        virtual ~resource_ptr()
        {
            acquired = ptr->liberate();
        }        
};

void stuff(int i)
{
    if(i % 2 == 0)
        throw i;
}

void f()
{
    resource_ptr ptr1(r1);
    resource_ptr ptr2(r2);        

    stuff(0);
}

int main()
{
    f();
}
