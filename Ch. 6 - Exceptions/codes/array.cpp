#include <iostream>
#include <string>
#include <stdexcept>

using namespace std;

class array
{
    int n;
    int * tab;
    
    public:
        array(int nbr) : n(nbr), tab(new int[n]) {} //evil new for academic purposes
        
        array(const array& a) = delete;
        array & operator = (const array & v) = delete;
        
        ~array()
        {
            delete[] tab;
        }    
                
        int& at(int i)
        {
            rangeCheck(i);
            return tab[i];
        }

        int size() const
        {
            return n;
        }

    private:
        void rangeCheck(int i) const
        {
            if(i < 0 || i >= n)
            {
                string s = "out of range : size of ";
                s += to_string(n);
                s += " , accessed at ";
                s += to_string(i);
                throw out_of_range(s);
            }
        }
};

int main()
{
    array v(5);

    for(int i = 0; i < 5; i++)
        v.at(i) = i * i;

    for(int i = 0; i < 5; i++)
        cout << v.at(i) << endl;
    cout << endl;

    v.at(0) = 2; //try to remove & from []

    for(int i = 0; i < 5; i++)
        cout << v.at(i) << endl;
    
    try
    {
        v.at(-1) = 4; //try to remove rangeCheck
    }
    catch(const out_of_range& e)
    {
        cerr << e.what() << endl;
    }
}
