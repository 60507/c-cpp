#include <iostream>

using namespace std;
                                    //////////////////////////////////
struct exceptA {};                  /////////       A      ///////////
                                    /////////       |      ///////////
struct exceptB : exceptA {};        /////////       B      ///////////
                                    /////////       |      ///////////
struct exceptC : exceptB {};        /////////       C      ///////////
                                    //////////////////////////////////

void f()
{
    throw exceptB();        
    
    //Q: why not into catch(const int&) ? 
    //Try to remove catch(...)
    //try catch(int)
    //throw 1.;
    
    //throw true;
}
/*
 * 1. Normal run    //base
 * 2. Switch A & B  //exact
 * 3. Delete A & B  //any
 * 4. //subclass ???
 */
int main()
{
    try             
    {
        f();
        cout << "Fine" << endl;
    }
    catch(const exceptA& e)
    {
        cout << "I caught an A" << endl;
    }
    catch(const exceptB& e)
    {
        cout << "I caught a B" << endl;
    }
    catch(const exceptC& e)
    {
        cout << "I caught a C" << endl;
    }
    catch(const int& d)
    {
        cout << "I caught an int" << endl;
    }
    catch(...)
    {
        cout << "I caught something" << endl;
    }
}
